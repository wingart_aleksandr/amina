$(function(){
	var viewportmeta = document.querySelector && document.querySelector('meta[name="viewport"]'),
	ua = navigator.userAgent,

	gestureStart = function () {viewportmeta.content = "width=device-width, minimum-scale=0.25, maximum-scale=1.6";},

	scaleFix = function () {
		if (viewportmeta && /iPhone|iPad/.test(ua) && !/Opera Mini/.test(ua)) {
			viewportmeta.content = "width=device-width, minimum-scale=1.0, maximum-scale=1.0";
			document.addEventListener("gesturestart", gestureStart, false);
		}
	};
	
	scaleFix();
});
var ua=navigator.userAgent.toLocaleLowerCase(),
 regV = /ipod|ipad|iphone/gi,
 result = ua.match(regV),
 userScale="";
if(!result){
 userScale=",user-scalable=0"
}
document.write('<meta name="viewport" content="width=device-width,initial-scale=1.0'+userScale+'">')

// ЕСЛИ ПРОЕКТ РЕСПОНСИВ ТО ВСЕ ЧТО ВЫШЕ НУЖНО РАССКОМЕНТИРОВАТЬ. СКРИПТ ВЫШЕ ПРЕДНАЗНАЧЕН ДЛЯ КОРРЕКТНОГО ОТОБРАЖЕНИЯ ВЕРСТКИ ПРИ СМЕНЕ ОРИЕНТАЦИИ НА ДЕВАЙСАХ



if($(".data-mh").length){
  include("js/jquery.matchHeight-min.js");
}
if($(".sf-menu").length){
  include("js/superfish.js");
  include("js/hoverIntent.js");
}
if($("#client_slideshow").length){
  include("js/jquery.royalslider.min.js");
}
if($(".owl-carousel").length){
    include('js/owl.carousel.js');
}
if($(".openModalBtn").length){
    include('js/jquery.arcticmodal.js');
}
if($(".flexslider").length){
    include('js/jquery.flexslider.js');
}
if($("#map-canvas").length){
    include('http://maps.googleapis.com/maps/api/js?sensor=false');
    include('js/gmaps.js');
}
    include('js/jquery.easing.1.3.js');
  	include("js/modernizr.js");



function include(url){ 
  document.write('<script src="'+ url + '"></script>'); 
}




$(document).ready(function(){

  // ДЕЛАЕМ ИНИТ ФУНКЦИЙ ЗДЕСЬ

  $("#resp_nav_btn").on("click", function(){
    $("body").toggleClass("show_menu");
    // if("body" !== $(".show_menu")){
    // 	var liMenu = $(".menu_item"); 
    // 		liMenu.removeClass("active");
    // 		liMenu.find(".dropdown-menu").hide();
    // }
  })

  if($(window).width() <= 995){
    $(document).on("click touchstart", function(event) {
      if ($(event.target).closest(".show_menu nav").length) return;
      $("body").removeClass("show_menu");
      event.stopPropagation();
    })
  }


  $(".openModalBtn").on("click", function(){
  	var currentModal = $(this).attr("href");
  	$(currentModal).arcticmodal({
		  afterOpen: function(){
		    if($(currentModal).find('.owl-carousel').length){
				$(currentModal).find('.owl-carousel').owlCarousel({
					navigation:true,
					items : 1
				});
		    }
		  }
		});
  	event.preventDefault();
  });

  
   	// Homepage slider
   	if($("#client_slideshow").length){
	   $('#client_slideshow').royalSlider({
		    fullscreen: false,
		    controlNavigation: 'bullets',
		    autoScaleSlider: true, 
		    autoScaleSliderWidth: 1872, 
		    autoScaleSliderHeight: 650,
		    loop: true,
		    imageScaleMode: 'none',
		    imageAlignCenter: false,
		    navigateByClick: true,
		    numImagesToPreload:2,
		    arrowsNav:true,
		    arrowsNavAutoHide: false,
		    arrowsNavHideOnTouch: true,
		    keyboardNavEnabled: true,
		    fadeinLoadedSlide: true,
		    sliderDrag:false,
		    globalCaption: false,
		    globalCaptionInside: false,
		    imgWidth: 1872,
		    transitionType:'move',
		    autoPlay: {
		      enabled: true,
		      pauseOnHover: false
		    },
		    block: {
		      delay: 400
		    },
		    bullets: {
		      controlsInside : true
		    },
		    thumbs: {
		      appendSpan: false,
		      firstMargin: true,
		      paddingBottom: 0,
		      fitInViewport:false,
		      spacing: 5
		    }
		});
	}
	// ============================flexslider start=======================
	if($(".flexslider").length){
	  $('#pasta_slider_mini').flexslider({
	    animation: "slide",
	    controlNav: false,
	    animationLoop: false,
	    slideshow: false,
	    itemWidth: 300,
	    itemMargin: 20,
	    asNavFor: '#pasta_slider_big'
	  });
	 
	  $('#pasta_slider_big').flexslider({
	    animation: "slide",
	    controlNav: false,
	    animationLoop: false,
	    slideshow: false,
	    sync: "#pasta_slider_mini"
	  });
	}
	// ============================flexslider end=======================

	var sticky = {

		init: function(){

			var self = this;
				self.w = $(window);
				self.menu = $('#primary_nav');
				self.dropdown = self.menu.find('.dropdown-menu');

			self.currentItem();

			self.w.on("resize",function(){
				self.currentItem();				
			});

		},

		scrollPart: function(id){

			var self = this;
				self.wWidth = self.w.width();

			var headerHeight = $("header").height(),
				offset = $(id).offset().top;	

			if(self.wWidth > 767){
				console.log(headerHeight);
				$('html, body').stop().animate({   //
				 	
				 	scrollTop: offset - headerHeight  //
				
				}, 800);
			}
			else{
				$('html, body').stop().animate({   //
				 	
				 	scrollTop: offset  //
				
				}, 800);
			}

		},

		currentItem: function(){

			var self = this;

			self.dropdown.on('click',"a",function(){
				
				event.preventDefault();

				var $this = $(this),
					id = $this.attr("href");
				self.menu.find("li").removeClass("current");
				$this.parent(".dropdown-menu-item").addClass("current");
				self.scrollPart(id)
			});

		},


	}

	sticky.init();

	
		

	// swipe on mobile start
	$(document).on( "swipeleft swiperight", "nav", function(e) {
		$("body").toggleClass("show_menu");
	});
	// swipe on mobile end

	// tooltip on click start
		if($(".biscuit_figure").length){ 

			var $biscuitBox = $("#biscuit_box").find(".biscuit_figure > .tooltip_box");

			$(".biscuit_figure").on("click", function(event){
				if($(event.target).closest(".close").length) return;

				var $this = $(this),
					$showBox = $this.find(".tooltip_box");
				$biscuitBox.removeClass("active");
			    $showBox.addClass("active");

			});


			$(".close").on("click", function(){
				var $this = $(this),
					$showBox = $this.parent();
			    $showBox.removeClass("active");
			});

			$(document).on("click", function(event) {
		      if ($(event.target).closest(".biscuit_figure").length) return;
		      $(".tooltip_box").removeClass("active");
		      event.stopPropagation();
		    });
			
		}
	// tooltip on click end

	//скрипт для того чтобы элементы правильно перестраивались на тач устройствах и на компе (он считывает сколько ширина скрола)
	  var scrollWidth;
	  function detectScrollBarWidth(){
	      var div = document.createElement('div');
	      div.className = "detect_scroll_width";
	      document.body.appendChild(div);
	      scrollWidth = div.offsetWidth - div.clientWidth;
	      document.body.removeChild(div);
	      // console.log(scrollWidth);
	  }
	    detectScrollBarWidth();
	  //скрипт для того чтобы элементы правильно перестраивались на тач устройствах и на компе (он считывает сколько ширина скрола) 
	  
	  if($(".header_bottom_menu").length){
	  		function blockPosition(){
		      var bodyWidth = $(window).width();
		      if(bodyWidth + scrollWidth <= 767 && $('body').hasClass('respond')){
		        $('.header_top').find(".header_right").append($('.head_bottom_menu'));
		        $('body').removeClass('respond');
		      }
		      else if(bodyWidth + scrollWidth > 767 && !$('body').hasClass('respond')){
		        $('header .header_bottom_menu').append($('.head_bottom_menu'));
		        $('body').addClass('respond');
		      }
		    }
		    blockPosition();

		    $(window).on('resize',function(){
		      setTimeout(function(){
		          blockPosition();
		      }, 500);
		    });
	  }

	  	if($("#map-canvas").length){
		  	// map
		    new GMaps({
		        div: '#map-canvas',
		        lat: mapLat,
		        lng: mapLong
		    });

		    map = new GMaps({
		        div: '#map-canvas',
		        lat: mapLat,
		        lng: mapLong,
		        "visibility": "off",
		        zoom:16,
		        panControl: false,
		        zoomControl: true,
		        mapTypeControl: false,
		        scrollwheel: false
		    });
		    map.addMarker({
		        lat: mapLat,
		        lng: mapLong,
		        icon: 'http://amina.co/wp-content/themes/amina/amina/images/map_marker.png' 
		    });
		    
		    // map end

	  	}

	  	$('.menu_link').on('click',function(event){

	        var windowWidth = $(window).width(),
	            $parent = $(this).parents('.menu_item');

	        if(windowWidth > 767){
	          if($("html").hasClass("md_touch")){
	            if(!$parent.hasClass('active')){

	              event.preventDefault();

	              $parent.toggleClass('active')
	               .siblings()
	               .find('.menu_link')
	               .removeClass('active');
	            }
	          }  
	        }
	        
	        else{
	            
	          if(!$parent.hasClass('active')){

	            event.preventDefault();

	            $parent.toggleClass('active')
	             .siblings()
	             .removeClass('active');
	            $parent.find(".dropdown-menu")
	             .slideToggle()
	             .parents('.menu_item')
	             .siblings()
	             .find("dropdown-menu")
	             .slideUp();
	          }
	        }
	    });
		

});



$(window).load(function() {
	// Скрипт для перехода на другую страницу сразу в якорь начало
	// setTimeout(function(){

		var hash = window.location.hash;
		if(hash !== ""){
			var	header = $("header").height(),
				offset =  $(hash).offset().top;
			if(!hash) return;

			$('html, body').stop().animate({   
				
			 	scrollTop: $(window).width() > 767 ? offset - header : offset
			
			}, 800);
		}

		// }, 500);


	// Скрипт для перехода на другую страницу сразу в якорь конец


});